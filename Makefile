ADF_FILE=6issue_fc_streamout.adf
OPT=-O3 -g

BINARY_NAME=asip_decompress

CFLAGS=-I../snappy-c -I../lz4joost

#run make X86=yesplease to compile for x86
ifndef X86
CFLAGS+=-Wall ${OPT} -a ${ADF_FILE} -d -v ${TCE_STREAM} ${ADDRESS_SPACE}
LDFLAGS += -a ${ADF_FILE} -d -v
CC=tcecc
endif

ifndef TCE_STREAM_DISABLED
TCE_STREAM=-DTCE_STREAM
else
ifdef SEPARATE_ADDRESS_SPACE
ADDRESS_SPACE=-DSEPARATE_ADDRESS_SPACE
endif
endif


.PHONY: all
all: $(BINARY_NAME) data.compressed.snfr data.compressed.lz4
	./$(BINARY_NAME) < data.compressed.lz4 > data.decompressed.lz4.out
	./$(BINARY_NAME) < data.compressed.snfr > data.decompressed.snfr.out
	md5sum data.decompressed data.decompressed.lz4.out
	md5sum data.decompressed data.decompressed.snfr.out

$(BINARY_NAME): main.c ../lz4joost/lz4.c ../snappy-c/snappy-framed.c
	${CC} $(OPT) ${CFLAGS} $^ -o $@

data.compressed.lz4: data.decompressed
	lz4 -B4 -BD --no-frame-crc $< $@ #-B is block size, 4 is 64k. Default is 7, 256k. -BD is enable block dependency

data.compressed.snfr: data.decompressed
	snzip < $< > $@

data.decompressed:
	dd if=/dev/urandom of=$@.raw bs=1k count=100
	hexdump -C $@.raw > $@

.PHONY: clean
clean:
	rm -f data.decompressed data.compressed.lz4 data.compressed.sn data.decompressed.sn.out data.decompressed.lz4.out data.decompressed.raw $(BINARY_NAME)

